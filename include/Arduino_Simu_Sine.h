/*
 * Arduino_Simu_Sine.h
 *
 *  Created on: Feb 16, 2015
 *      Author: sherpa
 */

#ifndef ARDUINO_SIMU_SINE_H_
#define ARDUINO_SIMU_SINE_H_

unsigned int getSineValue( ) ;

#endif /* ARDUINO_SIMU_SINE_H_ */
